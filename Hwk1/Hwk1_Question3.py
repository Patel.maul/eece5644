import matplotlib.pyplot as plt # For general plotting
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d
import numpy as np

from scipy.stats import multivariate_normal # MVN not univariate
from sklearn.metrics import confusion_matrix
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

np.set_printoptions(suppress=True)

def perform_pca(X):
    """  Principal Component Analysis (PCA) on real-valued vector data.
    Args:
        X: Real-valued matrix of samples with shape [N, n], N for sample count and n for dimensionality.
    Returns:
        U: An orthogonal matrix [n, n] that contains the PCA projection vectors, ordered from first to last.
        D: A diagonal matrix [n, n] that contains the variance of each PC corresponding to the projection vectors.
        Z: PC projection matrix of the zero-mean input samples, shape [N, n].
    """

    # First derive sample-based estimates of mean vector and covariance matrix:
    mu = np.mean(X, axis=0)
    sigma = np.cov(X.T)

    # Mean-subtraction is a necessary assumption for PCA, so perform this to obtain zero-mean sample set
    C = X - mu

    # Get the eigenvectors (in U) and eigenvalues (in D) of the estimated covariance matrix
    lambdas, U = np.linalg.eig(sigma)
    # Get the indices from sorting lambdas in order of increasing value, with ::-1 slicing to then reverse order
    idx = lambdas.argsort()[::-1]
    # Extract corresponding sorted eigenvectors and eigenvalues
    U = U[:, idx]
    D = np.diag(lambdas[idx])

    # PC projections of zero-mean samples, U^Tx (x mean-centred), matrix over N is XU
    Z = C.dot(U)

    # If using sklearn instead:
    # pca = PCA(n_components=X.shape[1])  # n_components is how many PCs we'll keep... let's take all of them
    # X_fit = pca.fit(X)  # Is a fitted estimator, not actual data to project
    # Z = pca.transform(X)

    return U, D, Z


# Set seed to generate reproducible "pseudo-randomness" (handles scipy's "randomness" too)
np.random.seed(6)

plt.rc('font', size=22)          # controls default text sizes
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=18)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=14)    # fontsize of the tick labels
plt.rc('ytick', labelsize=14)    # fontsize of the tick labels
plt.rc('legend', fontsize=18)    # legend fontsize
plt.rc('figure', titlesize=22)  # fontsize of the figure title

file = open("Machine_LearningHWK/winequality-white.csv")

winedata = np.genfromtxt(file, delimiter=";")
winedata = winedata[1:,:]

rows = winedata.shape[0]
cols = winedata.shape[1]

total = 0
count = 0

#for i in X:
#    print(i)
#    if i[11] == 3: #check if quality is 3
#        total = total + i[0]
#        count = count + 1

print(count)
print(rows, ' * ',cols)
print(winedata[0][0])

class_labels = winedata[:,11]
sample_ct = len(winedata)
winedata = winedata[:,:11]
#print(winedata[4000])

labels_ct = winedata.shape[1]
quality_ct=7 #only 7 varying qualities in the data set
print(labels_ct)

quals = ([3, 4, 5, 6, 7, 8, 9])
#print(quals)

priors=np.zeros(quality_ct)
print(priors)

mu=np.zeros((quality_ct,labels_ct))
Sigma=np.zeros((quality_ct,labels_ct,labels_ct))

print(mu)

for x in quals:
    count = (class_labels == x).sum()
    priors[x-3]=count/sample_ct
    mu[x-3]=np.mean(winedata[class_labels==x,:], axis=0)
    Sigma[x-3]=np.cov(winedata[class_labels==x,:],rowvar=False)+0.01*np.identity(labels_ct)

###################################################################

# Min prob. of error classifier
# Conditional likelihoods of each class given x, shape (C, N)
class_cond_likelihoods = np.array([multivariate_normal.pdf(winedata, mu[quals,:], Sigma[quals,:,:]) for quals in range(quality_ct)])
# Take diag so we have (C, C) shape of priors with prior prob along diagonal
class_priors = np.diag(priors)
# class_priors*likelihood with diagonal matrix creates a matrix of posterior probabilities
# with each class as a row and N columns for samples, e.g. row 1: [p(y1)p(x1|y1), ..., p(y1)p(xN|y1)]
class_posteriors = class_priors.dot(class_cond_likelihoods)

# MAP rule, take largest class posterior per example as your decisions matrix (N, 1)
# Careful of indexing! Added np.ones(N) just for difference in starting from 0 in Python and labels={1,2,3}
decisions = np.argmax(class_posteriors, axis=0) + 3*np.ones(sample_ct) 

# Simply using sklearn confusion matrix
print("Confusion Matrix (rows: Predicted class, columns: True class):")
conf_mat = confusion_matrix(decisions, class_labels)
print(conf_mat)
correct_class_samples = np.sum(np.diag(conf_mat))
print("Total Mumber of Misclassified Samples: {:d}".format(sample_ct - correct_class_samples))

# Alternatively work out probability error based on incorrect decisions per class
# perror_per_class = np.array(((conf_mat[1,0]+conf_mat[2,0])/Nl[0], (conf_mat[0,1]+conf_mat[2,1])/Nl[1], (conf_mat[0,2]+conf_mat[1,2])/Nl[2]))
# prob_error = perror_per_class.dot(Nl.T / N)

prob_error = 1 - (correct_class_samples / sample_ct)
print("Empirically Estimated Probability of Error: {:.4f}".format(prob_error))

# Plot for decisions vs true labels
fig = plt.figure(figsize=(10, 10))

L=quals
marker_shapes = '.,ov^<>'
for r in L: # Each decision option
    for c in L: # Each class label
        ind_rc = np.argwhere((decisions==r) & (class_labels==c))
        marker = marker_shapes[r-4]
        if r == c:
            plt.plot(winedata[ind_rc, 0], winedata[ind_rc, 1], marker+'g')
        else:
            plt.plot(winedata[ind_rc, 0], winedata[ind_rc, 1], marker+'r')

#plt.legend()
plt.xlabel(r"$x_1$")
plt.ylabel(r"$x_2$")
plt.title("Classification Decisions: Marker Shape/Predictions, Color/True Labels")
plt.tight_layout()
plt.show()

X_GMM = winedata
# Perform PCA on transposed UMM variable X
_, _, Z = perform_pca(X_GMM)

# Add back mean vector to PC projections if you want PCA reconstructions
Z_GMM = Z + np.mean(X_GMM, axis=0)
#print(Z_UMM.shape)
marker_shapes = '.,ov^<>'
for r in L: # Each decision option
    for c in L: # Each class label
        ind_rc = np.argwhere((decisions==r) & (class_labels==c))

        # Decision = Marker Shape; True Labels = Marker Color
        marker = marker_shapes[r-4]
        if r == c:
            plt.plot(Z_GMM[ind_rc, 0], Z_GMM[ind_rc, 1], marker+'g')
        else:
            plt.plot(Z_GMM[ind_rc, 0], Z_GMM[ind_rc, 1], marker+'r')

plt.legend()
plt.xlabel(r"$x_1$")
plt.ylabel(r"$x_2$")
plt.title("Classification Decisions: Marker Shape/Predictions, Color/True Labels")
plt.tight_layout()
plt.show()





################ part B #######################################


Xtrain=np.loadtxt("Machine_LearningHWK/X_train.txt")
Ytrain=np.loadtxt("Machine_LearningHWK/y_train.txt")


length=len(Xtrain)

labels_ct = Xtrain.shape[1]
quality_ct=6 #only 6 varying qualities in the data set
print(labels_ct)

quals = ([1, 2, 3, 4, 5, 6])
#print(quals)

priors=np.zeros(quality_ct)
print(priors)
mu = np.zeros((quality_ct, labels_ct))
Sigma = np.zeros((quality_ct, labels_ct, labels_ct))

priors=np.zeros(quality_ct)

for i in quals:
    count = (Ytrain == i).sum()
    priors[i-1]=count/length
    mu[i-1]=np.mean(Xtrain[Ytrain==i,:], axis=0)
    Sigma[i-1]=np.cov(Xtrain[Ytrain==i,:],rowvar=False)+0.01*np.identity(labels_ct)


# Min prob. of error classifier
# Conditional likelihoods of each class given x, shape (C, N)
class_cond_likelihoods = np.array([multivariate_normal.pdf(Xtrain, mu[quals,:], Sigma[quals,:,:]) for quals in range(quality_ct)])
# Take diag so we have (C, C) shape of priors with prior prob along diagonal
class_priors = np.diag(priors)
# class_priors*likelihood with diagonal matrix creates a matrix of posterior probabilities
# with each class as a row and N columns for samples, e.g. row 1: [p(y1)p(x1|y1), ..., p(y1)p(xN|y1)]
class_posteriors = class_priors.dot(class_cond_likelihoods)

# MAP rule, take largest class posterior per example as your decisions matrix (N, 1)
# Careful of indexing! Added np.ones(N) just for difference in starting from 0 in Python and labels={1,2,3}
decisions = np.argmax(class_posteriors, axis=0) + np.ones(length) 

# Simply using sklearn confusion matrix
print("Confusion Matrix (rows: Predicted class, columns: True class):")
conf_mat = confusion_matrix(decisions, Ytrain)
print(conf_mat)
correct_class_samples = np.sum(np.diag(conf_mat))
print("Total Mumber of Misclassified Samples: {:d}".format(length - correct_class_samples))

# Alternatively work out probability error based on incorrect decisions per class
# perror_per_class = np.array(((conf_mat[1,0]+conf_mat[2,0])/Nl[0], (conf_mat[0,1]+conf_mat[2,1])/Nl[1], (conf_mat[0,2]+conf_mat[1,2])/Nl[2]))
# prob_error = perror_per_class.dot(Nl.T / N)

prob_error = 1 - (correct_class_samples / length)
print("Empirically Estimated Probability of Error: {:.4f}".format(prob_error))

# Plot for decisions vs true labels
fig = plt.figure(figsize=(10, 10))

for r in quals: 
    for c in quals: 
        ind_rc = np.argwhere((decisions==r) & (Ytrain==c))
        marker = marker_shapes[r-4]
        if r == c:
            plt.plot(Xtrain[ind_rc, 0], Xtrain[ind_rc, 1], marker+'g')
        else:
            plt.plot(Xtrain[ind_rc, 0], Xtrain[ind_rc, 1], marker+'r')
plt.xlabel(r"$x_1$")
plt.ylabel(r"$x_2$")
plt.title("Classification Decisions: Marker Shape/Predictions, Color/True Labels")
plt.tight_layout()
plt.show()


X_GMM = Xtrain
# Perform PCA on transposed UMM variable X
_, _, Z = perform_pca(X_GMM)

# Add back mean vector to PC projections if you want PCA reconstructions
Z_GMM = Z + np.mean(X_GMM, axis=0)
#print(Z_UMM.shape)
marker_shapes = '.,ov^<>'
for r in L: # Each decision option
    for c in L: # Each class label
        ind_rc = np.argwhere((decisions==r) & (Ytrain==c))
        marker = marker_shapes[r-4]
        if r == c:
            plt.plot(Z_GMM[ind_rc, 0], Z_GMM[ind_rc, 1], marker+'g')
        else:
            plt.plot(Z_GMM[ind_rc, 0], Z_GMM[ind_rc, 1], marker+'r')

plt.legend()
plt.xlabel(r"$x_1$")
plt.ylabel(r"$x_2$")
plt.title("Classification Decisions: Marker Shape/Predictions, Color/True Labels")
plt.tight_layout()
plt.show()

